Shader "Custom/Atmosphere" {

Properties
{
	_Tex1 ("Texture1", 2D) = "white" {} // ��������
	
	_Color("Main Color", COLOR) = (0,0,0,0) // ���� �����������
	_Height("Height", Range(0,20)) = 0.5 // ����������

}
//���������
SubShader
{
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	ZWrite Off
    Blend SrcAlpha OneMinusSrcAlpha
    Cull front 
	
	
	LOD 100 // ����������� ������� �����������

	Pass
	{
		CGPROGRAM
		#pragma vertex vert // ��������� ��� ��������� ������
		#pragma fragment frag // ��������� ��� ��������� ����������

		#include "UnityCG.cginc" // ���������� � ��������� ���������

		sampler2D _Tex1; // ��������
		float4 _Tex1_ST;


		float4 _Color; // ����, ������� ����� ������������ �����������
		float _Height; // ����������

		struct v2f	// ���������, ������� �������� ������������� ������ ������� � ������ ���������
	{
	float2 uv : TEXCOORD0; // UV-���������� �������
	float4 vertex : SV_POSITION; // ���������� �������
	};


	v2f vert (appdata_full v)//����� ���������� ��������� ������
{
	v2f result;
	
	
	v.vertex.xyz += v.normal * _Height;


	result.vertex = UnityObjectToClipPos(v.vertex);
	result.uv = TRANSFORM_TEX(v.texcoord, _Tex1);
	return result;
}

fixed4 frag(v2f i) : SV_Target//����� ���������� ��������� ��������, ���� �������� ���������� �� ���� ���������
{
	fixed4 color;
	
	color =  _Color;
	return color;
}
ENDCG
		}
	}
}