using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Characters
{

    public class ChooseName : MonoBehaviour
    {
        
        [SerializeField] private Button button;
        [SerializeField] private TMP_InputField InputField;
        [SerializeField] GameObject SpaceShipPrefab;
       // public string Name;
        

        public void ChangeName()
        {
           // Name = InputField.text;
            button.gameObject.SetActive(false);
            InputField.gameObject.SetActive(false);
            
            SpaceShipPrefab.GetComponent<ShipController>().name = InputField.text;
        }






    }
}